#include <iostream>

struct Node {
  struct Node* next;
  int val;
};

void reverseAfter(struct Node* head, int val);
void reverseAfter2(struct Node* head, int val);

void createList(Node*& head) {
  size_t node_size = sizeof(*head);
  Node* tmp = nullptr;
  for (int i = 0; i < 13; ++i) {
    tmp = reinterpret_cast<Node*>(operator new(node_size));
    tmp->val = i + 1;
    tmp->next = head;
    head = tmp;
  }
}
void show_list(const Node* const& head) {
  for (auto tmp = head; tmp; tmp = tmp->next) std::cout << tmp->val << ' ';
  std::cout << std::endl;
}

int main() {
  Node* head = nullptr;
  createList(head);
  show_list(head);
  reverseAfter2(head, 5);
  show_list(head);
}

Node* reverse(Node* head, Node*& tail) {
  if (!head->next) {
    tail->next = head;
    return head;
  }
  reverse(head->next, tail)->next = head;
  head->next = nullptr;
  return head;
}

void reverseAfter2(struct Node* head, int val) {
  if (!head) return;
  for (; head->next && head->next->val != val; head = head->next)
    ;
  if (!head->next) return;
  reverse(head->next, head);
}

void reverseAfter(struct Node* head, int val) {
  if (!head) return;
  for (; head->next && head->next->val != val; head = head->next)
    ;
  if (!head->next) return;
  if (!head->next->next) return;
  Node *last = head->next->next, *end = last->next;
  for (; end; end = end->next, last = last->next)
    ;
  while (head->next != last) {
    last->next = head->next;
    head->next = last->next->next;
    last->next->next = end;
    end = last->next;
  }
}
