#include <iostream>

struct triangel {
  unsigned short const* const ids;
  int* const connected_edges;

 public:
  triangel(unsigned short const* const& index_array,
           int* const& connected_array)
      : ids{index_array}, connected_edges{connected_array} {
    connected_edges[0] = -1;
    connected_edges[1] = -1;
    connected_edges[2] = -1;
  }
  bool is_all_connected() const noexcept {
    return (connected_edges[0] + connected_edges[1] + connected_edges[2] >= 0)
               ? true
               : false;
  }
  void findConnectivity(const triangel& obj) noexcept {
    if (is_all_connected()) return;
    for (int i = 0, j = 0, k = 0; i < 3; i++) {
      if (i > 1)
        k = 2;
      else
        j++;
      for (int ii = 0, ij = 0, ik = 0; ii < 3; ii++) {
        if (ii > 1)
          ik = 2;
        else
          ij++;
        if ((ids[i - k] == obj.ids[ii - ik] && ids[j] == obj.ids[ij]) ||
            (ids[i - k] == obj.ids[ij] && ids[j] == obj.ids[ii - ik])) {
          connected_edges[i] = ids[3 + k - j - i];
          obj.connected_edges[ii] = obj.ids[3 - ij - ii + ik];
        }
      }
    }
    return;
  }
};

void findConnectivity(unsigned short* indexTriples, int T,
                      int* connectivityOut) {
  auto ts = reinterpret_cast<triangel*>(operator new[](sizeof(triangel) * T));
  for (int i = 0; i < T; ++i)
    new (&ts[i]) triangel(indexTriples + i * 3, connectivityOut + i * 3);
  for (int i = 0; i < T; ++i) {
    for (int j = 1; j < T - i; ++j) {
      ts[i].findConnectivity(ts[i + j]);
    }
  }
  operator delete[](ts);
}

int main() {
  unsigned short a[] = {0, 2, 7, 4, 3, 5, 6, 2, 0};
  int count = sizeof(a) / sizeof(*a) / 3;
  int* c = new int[count * 3];
  findConnectivity(a, count, c);
  for (int i = 0; i < count * 3; i++) std::cout << c[i] << ' ';
  std::cout << std::endl;
  delete[] c;
}
