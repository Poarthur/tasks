#include <cassert>
#include <iostream>

#define MAX_INT_FACT 13

int factorial2(int n) {
  assert(n >= 0 && n < MAX_INT_FACT);
  if (!n) return 1;
  int result = n;
  while (n > 1) result *= --n;
  return result;
}

int factorial(int n) {
  assert(n >= 0 && n < MAX_INT_FACT);
  if (n < 2) return 1;
  return n * factorial(n - 1);
}

int main() {
  for (long long int i = 0; i < 100000000; i++) factorial(12);
  for (long long int i = 0; i < 100000000; i++) factorial2(12);
}
