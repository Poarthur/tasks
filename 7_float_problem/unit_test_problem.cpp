#include <cstdio>

bool equals(float f1, float f2, float f3) { return f1 == f2 && f2 == f3; }

bool testPasses(float f1, float f2, float f3) {
  return equals(f1, f2, f3) == (f1 - f2) == (f2 - f3);
}

void testRepro() {
  float a = 1.0f;  // fill in from value printed by above code
  float b = 1.0f;  // fill in from value printed by above code
  float c = 1.0f;  // fill in from value printed by above code
  int result = testPasses(a, b, c);
  printf("result %d", result);
};

int main() {
  float a, b, c;
  a = 1.0000001;
  b = 1.0000002;
  c = 1.0000003;
  if (testPasses(a, b, c)) {
    printf("Pass\n");
  } else  // someFunc fails, print the values so we can reproduce
  {
    printf("Fail: %f %f %f\n", a, b, c);
  }
  testRepro();
}
