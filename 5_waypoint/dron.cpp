#include <cassert>
#include <cmath>
#include <iostream>
#include <valarray>

#define PI 3.14159265

enum class Direction : unsigned char { REJECTED = 0, RIGHT, LEFT };

struct Vector3 {
  float x, y, z;

 public:
  Vector3();
  Vector3(const float&, const float&, const float&) noexcept;
  float length() const noexcept;
  Vector3 operator+(const Vector3& w) const noexcept;
  Vector3 operator-(const Vector3& w) const noexcept;
  float dot(const Vector3& w) const noexcept;
  Vector3 cross(const Vector3& w) const noexcept;
  Vector3 normalize() const noexcept;
  void show() { std::cout << x << ' ' << y << ' ' << z << std::endl; }
};

Vector3::Vector3() : x{}, y{}, z{} {}
Vector3::Vector3(const float& x, const float& y, const float& z) noexcept
    : x{x}, y{y}, z{z} {}
float Vector3::length() const noexcept {
  return std::sqrt(x * x + y * y + z * z);
}
Vector3 Vector3::operator+(const Vector3& w) const noexcept {
  return Vector3(x + w.x, y + w.y, z + w.z);
}
Vector3 Vector3::operator-(const Vector3& w) const noexcept {
  return Vector3(x - w.x, y - w.y, z - w.z);
}
float Vector3::dot(const Vector3& w) const noexcept {
  return x * w.x + y * w.y + z * w.z;
}
Vector3 Vector3::cross(const Vector3& w) const noexcept {
  return Vector3(y * w.z - z * w.y, z * w.x - x * w.z, x * w.y - y * w.x);
}
Vector3 Vector3::normalize() const noexcept {
  float len = length();
  if (!len) return Vector3();
  return Vector3(x / len, y / len, z / len);
}

// This is the method you must implement:
Direction testWayPoint(const Vector3& dronePosition,
                       const Vector3& droneForward,
                       const Vector3& targetWaypoint, const Vector3& worldUp) {
  Vector3 drone_to_point = targetWaypoint - dronePosition;
  float distance = drone_to_point.length();
  if (distance > 10) return Direction::REJECTED;
  float dron_point_dot = droneForward.dot(drone_to_point);
  if (dron_point_dot <= 0) return Direction::REJECTED;
  if (std::acos(droneForward.normalize().dot(drone_to_point.normalize())) *
          180.0 / PI >
      30)
    return Direction::REJECTED;
  Vector3 right_vector = droneForward.cross(worldUp);
  if (right_vector.dot(drone_to_point) > 0) return Direction::RIGHT;
  return Direction::LEFT;
}

int main() {
  Vector3 world_up(0, 0, 1);
  Vector3 drone_pos(0, 10, 0);
  Vector3 drone_forward(1, 0, 0);
  Vector3 point_pos(9, 11, 0);

  switch (testWayPoint(drone_pos, drone_forward, point_pos, world_up)) {
    case (Direction::REJECTED):
      std::cout << "REJECTED" << std::endl;
      break;
    case (Direction::LEFT):
      std::cout << "LEFT" << std::endl;
      break;
    case (Direction::RIGHT):
      std::cout << "RIGHT" << std::endl;
      break;
    default:
      std::cout << "DEFAULT" << std::endl;
  }
}
