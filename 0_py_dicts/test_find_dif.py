import unittest
import find_dif

class DictTest(unittest.TestCase):
    def test_find_dif(self):
        res = {'a':{'a1':1,'a2':2},'b':{'b1':{'b11':1,'b22':2}}}
        data = {'a':{'a1':1,'a2':2},'b':{'b1':{'b11':1,'b22':2}}}
        data['a']['a1'] = 3
        data['b']['b1']['b11'] = 5
        expected = {'a': {'a1': 3}, 'b': {'b1': {'b11': 5}}}
        find_dif.find_dif(res, data)
        self.assertEqual(res, expected)
        
        res = {'a':{'a1':1,'a2':2},'b':{'b1':{'b11':1,'b22':2}}}
        data = {'a':{'a1':1,'a2':2},'b':{'b1':{'b11':1,'b22':2}}}
        data['a']['a2'] = 3
        data['b']['b1']['b22'] = 5
        expected = {'a': {'a2': 3}, 'b': {'b1': {'b22': 5}}}
        find_dif.find_dif(res, data)
        self.assertEqual(res, expected)
        
if __name__ == '__main__':
    unittest.main()
