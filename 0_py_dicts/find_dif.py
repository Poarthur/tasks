res = {'a':{'a1':1,'a2':2},'b':{'b1':{'b11':1,'b22':2}}}

def find_dif(prev,curr):
    if not isinstance(prev, dict):
        return
    for i in prev.keys():
        if prev[i] == curr[i]:
            del prev[i]
        elif not isinstance(prev[i], dict):
            prev[i] = curr[i]
        else:
            find_dif(prev[i],curr[i])
    return

data = {'a':{'a1':1,'a2':2},'b':{'b1':{'b11':1,'b22':2}}}
data['a']['a1'] = 3
data['b']['b1']['b11'] = 5

find_dif(res,data)
print(res)

